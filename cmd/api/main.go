package main

import (
	"context"
	"poc-fx-hexagonal-arch/internal/bootstrap"
)

func main() {
	ctx := context.Background()
	bootstrap.Run(ctx)
}
