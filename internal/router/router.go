package router

import (
	"poc-fx-hexagonal-arch/internal/config"
	"poc-fx-hexagonal-arch/internal/health"

	"github.com/gin-gonic/gin"
	"gitlab.fretebras.com.br/trucker-journey/go-common-libs/healthcheck"
	"go.uber.org/fx"
	gintracer "gopkg.in/DataDog/dd-trace-go.v1/contrib/gin-gonic/gin"
)

var Module = fx.Options(
	fx.Provide(NewRouter),
)

type Router struct {
	*gin.Engine
}

func NewRouter(cfg config.Config, hc *health.HealthCheck) *Router {
	r := gin.New()

	// Add health check
	r.GET(healthcheck.EndpointChecker, gin.WrapH(hc.Handler()))

	// Setup router with middlewares
	rt := r.Group("/")
	rt.Use(gintracer.Middleware(cfg.App.Name))
	return &Router{r}
}
