package health

import (
	"context"
	"net/http"
	"poc-fx-hexagonal-arch/internal/config"
	"poc-fx-hexagonal-arch/internal/database"

	"gitlab.fretebras.com.br/trucker-journey/go-common-libs/healthcheck"
	"go.uber.org/fx"
)

var Module = fx.Options(
	fx.Provide(NewHealthCheck),
)

type Health interface {
	Handler() http.Handler
}

type HealthCheck struct {
	info   healthcheck.ServiceInfo
	checks []healthcheck.Check
}

func NewHealthCheck(cfg config.Config, db database.DB) *HealthCheck {
	hc := &HealthCheck{
		info: healthcheck.ServiceInfo{
			Name:    cfg.App.Name,
			Version: cfg.App.Version,
			Env:     cfg.App.Env,
		},
	}

	hc.addCheck("mongodb", func(ctx context.Context) error {
		return nil
	})

	return hc
}

type Check func(context.Context) error

func (h *HealthCheck) addCheck(name string, check Check) {
	h.checks = append(h.checks, healthcheck.Check{
		Name:    name,
		Checker: check,
	})
}

func (h *HealthCheck) Handler() http.Handler {
	return healthcheck.Handler(h.info, h.checks...)
}
