package database

import (
	"context"
	"poc-fx-hexagonal-arch/internal/config"

	"go.uber.org/fx"
)

var Module = fx.Options(
	fx.Provide(NewMongo),
)

type DB interface {
	Ping(context.Context) error
}

func NewMongo(cfg config.Config) DB {
	return nil
}
