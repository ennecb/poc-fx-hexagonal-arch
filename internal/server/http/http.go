package http

import (
	"context"
	"net/http"
	"poc-fx-hexagonal-arch/internal/config"
	"poc-fx-hexagonal-arch/internal/router"
	"time"

	"go.uber.org/fx"
)

var Module = fx.Options(
	fx.Provide(NewServer),
)

type Server interface {
	Start() error
	Stop(time.Duration) error
}

type httpServer struct {
	http *http.Server
}

func NewServer(cfg config.Config, router *router.Router) Server {
	return &httpServer{
		&http.Server{
			Addr:    cfg.Server.Addr,
			Handler: router,
		}}
}

func (s *httpServer) Start() error {
	return s.http.ListenAndServe()
}

func (s *httpServer) Stop(timeout time.Duration) error {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	return s.http.Shutdown(ctx)
}
