package repository

import (
	"poc-fx-hexagonal-arch/internal/database"

	"go.uber.org/fx"
)

var Module = fx.Options(
	fx.Provide(NewLocation),
)

type Location interface {
	Create() error
}

func NewLocation(db database.DB) Location {
	return nil
}
