package routes

import (
	"poc-fx-hexagonal-arch/internal/api/handlers"
	"poc-fx-hexagonal-arch/internal/router"

	"go.uber.org/fx"
)

var Module = fx.Options(
	fx.Provide(NewLocationRoutes),
	fx.Provide(NewRoutes),
)

type Route interface {
	Setup()
}

type Routes []Route

func NewRoutes(loc *LocationRoutes) Routes {
	return Routes{
		loc,
	}
}

func (r Routes) Setup() {
	for _, rr := range r {
		rr.Setup()
	}
}

type LocationRoutes struct {
	router  *router.Router
	handler *handlers.Location
}

func (lr *LocationRoutes) Setup() {
	lr.router.POST("/location", lr.handler.Create)
}

func NewLocationRoutes(r *router.Router, handler *handlers.Location) *LocationRoutes {
	return &LocationRoutes{
		router:  r,
		handler: handler,
	}
}
