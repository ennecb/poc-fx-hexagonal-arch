package handlers

import (
	"poc-fx-hexagonal-arch/internal/services"

	"github.com/gin-gonic/gin"
	"go.uber.org/fx"
)

var Module = fx.Options(
	fx.Provide(NewLocation),
)

type Location struct {
	svc services.Location
}

func NewLocation(svc services.Location) *Location {
	return &Location{
		svc: svc,
	}

}

func (l Location) Create(c *gin.Context) {

}
