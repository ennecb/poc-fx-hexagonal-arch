package bootstrap

import (
	"context"
	"poc-fx-hexagonal-arch/internal/api/handlers"
	"poc-fx-hexagonal-arch/internal/api/routes"
	"poc-fx-hexagonal-arch/internal/config"
	"poc-fx-hexagonal-arch/internal/database"
	"poc-fx-hexagonal-arch/internal/health"
	"poc-fx-hexagonal-arch/internal/repository"
	"poc-fx-hexagonal-arch/internal/router"
	server "poc-fx-hexagonal-arch/internal/server/http"
	"poc-fx-hexagonal-arch/internal/services"
	"time"

	"go.uber.org/fx"
)

func modules() fx.Option {
	return fx.Options(
		router.Module,
		server.Module,
		handlers.Module,
		routes.Module,
		services.Module,
		repository.Module,
		health.Module,
		database.Module,
		config.Module,
	)
}

func Run(ctx context.Context) {
	c := fx.New(
		modules(),
		fx.Invoke(setupRoutes),
		fx.Invoke(registerHooks),
	)

	if err := c.Start(ctx); err != nil {
		panic(err)
	}

	defer func() {
		if err := c.Stop(ctx); err != nil {
			panic(err)
		}
	}()
}

func setupRoutes(r routes.Routes) {
	r.Setup()
}

func registerHooks(lifecycle fx.Lifecycle, srv server.Server) {
	lifecycle.Append(
		fx.Hook{
			OnStart: func(ctx context.Context) error {
				return srv.Start()
			},
			OnStop: func(ctx context.Context) error {
				return srv.Stop(15 * time.Second)
			},
		},
	)
}
