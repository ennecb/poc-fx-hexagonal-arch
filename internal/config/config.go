package config

import (
	"go.uber.org/fx"
)

var Module = fx.Options(
	fx.Provide(Load),
)

type Server struct {
	Addr string
}

type App struct {
	Name    string
	Version string
	Env     string
}

type Config struct {
	App    App
	Server Server
}

func Load() (Config, error) {
	cfg := Config{
		App: App{
			Name:    "poc-fx-hexagonal-arch",
			Version: "1.0",
			Env:     "stage",
		},
		Server: Server{
			Addr: ":8089",
		},
	}
	return cfg, nil
}
