package services

import (
	"poc-fx-hexagonal-arch/internal/repository"

	"go.uber.org/fx"
)

var Module = fx.Options(
	fx.Provide(NewLocation),
)

type Location interface {
	Create() error
}

func NewLocation(repo repository.Location) Location {
	return nil
}
